/*
     File: RegionAnnotation.h
 Abstract: This is a custom MKAnnotation with the addition of region and radius properties for ease of use when using region monitoring.
  Version: 1.1

 */

#import <MapKit/MapKit.h>
#import "RegionAnnotationView.h"

@interface RegionAnnotation : NSObject <MKAnnotation> {

}

@property (nonatomic, retain) CLRegion *region;
@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;
@property (nonatomic, readwrite) CLLocationDistance radius;
@property (nonatomic, copy) NSString *title;
//@property (nonatomic, retain) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, retain) NSURL *url; // added extra from MapSearch

- (id)initWithCLRegion:(CLRegion *)newRegion Name:(NSString *) name;

@end
