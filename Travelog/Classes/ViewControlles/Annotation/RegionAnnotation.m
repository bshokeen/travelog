/*
     File: RegionAnnotation.m
 Abstract: This is a custom MKAnnotation with the addition of region and radius properties for ease of use when using region monitoring.
  Version: 1.1
 
 */

#import "RegionAnnotation.h"

@implementation RegionAnnotation

@synthesize region, coordinate, radius, subtitle, title;

- (id)init {
	self = [super init];
	if (self != nil) {
		self.title = @"Monitored Region";
	}
	
	return self;	
}


- (id)initWithCLRegion:(CLRegion *)newRegion Name:(NSString *) locName{
	self = [self init];
	
	if (self != nil) {
		self.region = newRegion;
		self.coordinate = region.center;
		self.radius = region.radius;
//		self.title = region.identifier;
        self.title = locName;
	}		

	return self;		
}


/*
 This method provides a custom setter so that the model is notified when the subtitle value has changed.
 */
- (void)setRadius:(CLLocationDistance)newRadius {
	[self willChangeValueForKey:@"subtitle"];
	
	radius = newRadius;
	
	[self didChangeValueForKey:@"subtitle"];
}


- (NSString *)title {
    return title;
}

- (NSString *)subtitle {
	return [NSString stringWithFormat: @"Lat: %.4F, Lon: %.4F, Rad: %.1fm", coordinate.latitude, coordinate.longitude, radius];	
}


@end
