/*
     File: RegionAnnotationView.h
 Abstract: This is a custom MKAnnotationView that handles updating and removing the radius overlay to show where the region surrounding an annotation is.
  Version: 1.1
 
 
 */

#import <MapKit/MapKit.h>

@class RegionAnnotation;

@interface RegionAnnotationView : MKPinAnnotationView {	
@private
	MKCircle *radiusOverlay;
	BOOL isRadiusUpdated;
}

@property (nonatomic, assign) MKMapView *map;
@property (nonatomic, assign) RegionAnnotation *theAnnotation;

- (id)initWithAnnotation:(id <MKAnnotation>)annotation;
- (void)updateRadiusOverlay;
- (void)removeRadiusOverlay;

@end