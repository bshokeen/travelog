//
//  AppConstants.m
//  Travelog
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "AppConstants.h"

@implementation AppConstants

NSString * const PREFS_MY_CONSTANT = @"prefs_my_constant";

// Added for cloud integration
NSString * const APP_ICLOUD_APPID          = @"4A4QA9AFG.com.dasheri.Travelog";
NSString * const APP_NAME                  = @"Travelog";
NSString * const APP_SQLITE_DBNAME         = @"Travelog.sqlite";
NSString * const APP_DATANOSYNC_FOLDERNAME = @"TravelogData.nosync";
NSString * const APP_DATA_LOG_FOLDERNAME   = @"TravelogLogs";

// Default Settings Plist File
NSString * const APP_PLISTNAME             = @"Travelog.plist";

@end
