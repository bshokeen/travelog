//
//  AppDelegate.h
//  Travelog
//
//  Created by Shokeen, Balbir on 12/7/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) UITabBarController *tabBarController ;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

