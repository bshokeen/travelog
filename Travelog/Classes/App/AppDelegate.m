//
//  AppDelegate.m
//  Travelog
//
//  Created by Shokeen, Balbir on 12/7/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import "AppDelegate.h"
#import "DetailViewController.h"
#import "MasterViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "AppUtilities.h"
#import "LocationViewController.h"
#import "Flurry.h"
#import "AppConstants.h"


@interface AppDelegate () <UISplitViewControllerDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    /*
    // Override point for customization after application launch.
    UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
    UINavigationController *navigationController = [splitViewController.viewControllers lastObject];
    navigationController.topViewController.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem;
    splitViewController.delegate = self;

    UINavigationController *masterNavigationController = splitViewController.viewControllers[0];
    MasterViewController *controller = (MasterViewController *)masterNavigationController.topViewController;
    controller.managedObjectContext = self.managedObjectContext;
    */
    
    // Flurry Integration Code
    [Flurry setCrashReportingEnabled:YES];
    //note: iOS only allows one crash reporting tool per app; if using another, set to: NO
#if DEBUG == 0
    [Flurry startSession:@""];
#else
    [Flurry startSession:@"D9GFGZ9YWSBSHFJ3XMJ5"];
#endif
    
    // My App Code starts here
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0; // Temporary

    //  LocationViewController *locationController = (LocationViewController *) self.window.rootViewController;
    
    
    /* if(YES) {
     LocationDetailViewController *loginVC = [[LocationDetailViewController alloc] initWithStyle:UITableViewStylePlain];
     
     UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
     
     [navigationController pushViewController:loginVC animated:YES];
     }*/
    [AppSharedData getInstance]; // Invoke once during launch to get it intialized beforehand.
    
    // BEFORE APPLICATION STARTS UP, RUNONCE Operations like performing the upgrade.
    [AppUtilities runOnceOperations];
    
    // Before loading initialize the required controllers
    
    // Once AppSharedData is initialized, check if there are any logentry events exist, if not point user to locations Tab
    if ( [[AppSharedData getInstance].appLocationMgr getAllRegionsCount] <= 0 ) {
        self.tabBarController = (UITabBarController *) self.window.rootViewController;
        [self.tabBarController setSelectedIndex:LOCATION_S_TAB_INDEX];
    }
    
    // Perform On load operations
    [self handleNotification: (NSDictionary *) launchOptions];
    

    UIUserNotificationType types = UIUserNotificationTypeSound | UIUserNotificationTypeBadge | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    

    return YES;
}




#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}
#endif

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
        [self saveContext];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0; //Clear the Badge No.
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [self saveContext];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0; //Clear the Badge No.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0; //Clear the Badge No.
    
    if ([[AppSharedData getInstance].appSettings autoDeleteOldLogs] == 1 )  {
        [CoreDataOperations deleteOldLogs:DAYS_90];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0; //Clear the Badge No.
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Split view

- (BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController ontoPrimaryViewController:(UIViewController *)primaryViewController {
    if ([secondaryViewController isKindOfClass:[UINavigationController class]] && [[(UINavigationController *)secondaryViewController topViewController] isKindOfClass:[DetailViewController class]] && ([(DetailViewController *)[(UINavigationController *)secondaryViewController topViewController] detailItem] == nil)) {
        // Return YES to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.dasheri.Travelog" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:APP_NAME withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:APP_SQLITE_DBNAME];
   
    NSDictionary *optionsDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                                       [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    NSError *error = nil;
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSLog(@"Path: %@", storeURL);
    
    
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:optionsDictionary error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


#pragma mark - additional api's added

-(void) handleNotification: (NSDictionary *) launchOptions {
    /* Commented out handling of notification as testnotification was coming up as a popup message
     // Handle launching from a notification
     UILocalNotification *notif = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
     
     // If not nil, the application was based on an incoming notifiation
     if (notif)
     {
     ULog(@"Notification initiated app startup");
     
     // Access the payload content
     NSLog(@"Notification payload: %@", [notif.userInfo objectForKey:@"payload"]);
     
     // Perform required application specific operations
     
     // As alert has fired now handle the reset of the alert, update the notification item in the database.
     
     }
     
     // On start of the application the alert badge count to be updated from the database.
     */
}


#pragma mark - addition default method implementations
- (void)application:(UIApplication *)app didReceiveLocalNotification:(UILocalNotification *)notif {
    /* NO ACTION REQUIRED FOR NOTIFICATION
     
     // Handle the notificaton when the app is running
     NSLog(@"Recieved Notification %@",notif);
     
     NSLog(@"Show the list of notifications...");
     // Get list of local notifications
     NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
     for (UILocalNotification *item in notificationArray) {
     NSLog(@"Notif Body: %@, fire: %@ repeat:%d", item.alertBody, [item.fireDate description], item.repeatInterval);
     }
     
     AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
     AudioServicesPlaySystemSound(1005);
     
     // Access the payload content
     //	NSLog(@"Notification payload: %@", [notification.userInfo objectForKey:@"payload"]);
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Task Reminder", @"Task Reminder")
     message:notif.alertBody
     delegate:self cancelButtonTitle:OK
     otherButtonTitles:nil];
     [alert show];
     
     // As alert has fired now handle the reset of the alert
     
     NSLog(@"Incoming notification in running app");
     */
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSLog(@"Error in registration. Error: %@", err);
}

@end
