//
//  main.m
//  Travelog
//
//  Created by Shokeen, Balbir on 12/7/14.
//  Copyright (c) 2014 Dasherisoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "AppConstants.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
