Time is money, take control of your time and see where you spending it! 
Here is a beautiful app which allows you to track your time expense. 
No need to keep starting or stopping the time tracker it does it automatically for you, based on your movement using GPS/geolocation.

You can track the time you spend at Gym, Office, Customer Site, Friends and many more ways to use it as you need.

Features:
• Automatically time log every time you visit a place
• Define which places you would like to track time at
• See your Regions at a glance